﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace KinectDragDrop
{
    public partial class Window1 : Window
    {

        private Dictionary<string, Expander> panelDictionary = null;

        Dictionary<string, Border> menuDictionary = null;

        public Window1()
        {
            InitializeComponent();
            this.panelDictionary = new Dictionary<string, Expander>()
            {
                {"buildings", this.expBuildings},
                {"trees", this.expTrees},
                {"lights", this.expLights},
                {"cars", this.expCars},
            };

            this.menuDictionary = new Dictionary<string,Border>()
            {
               {"Menu0", this.Menu0},
               {"Menu1", this.Menu1},
               {"Menu2", this.Menu2},
               {"Menu3", this.Menu3},
               {"Menu4", this.Menu4},
               {"ToolBox", this.ToolBox},
            };

        }

        public void HideAllMenus()
        {
            foreach (var menu in this.menuDictionary.Values)
            {
                menu.Visibility = Visibility.Hidden;
            }
        }

        private void MenuButtonClicked(object sender, RoutedEventArgs e)
        {
            string tag = (string)((Button)sender).Tag;
            foreach (KeyValuePair<string, Expander> pair in this.panelDictionary)
            {
                if (pair.Key.Equals(tag))
                {
                    pair.Value.Visibility = Visibility.Visible;
                }
                else
                {
                    pair.Value.Visibility = Visibility.Collapsed;
                }
            }
            this.Menu4.Visibility = Visibility.Hidden;
            this.ToolBox.Visibility = Visibility.Visible;
        }

        private void ButtonClicked(object sender, RoutedEventArgs e)
        {
            string tag = (string)((Button)sender).Tag;
            if (null != tag && this.menuDictionary.ContainsKey(tag))
            {
                HideAllMenus();
                this.menuDictionary[tag].Visibility = Visibility.Visible;
            }
        }

    }
}
