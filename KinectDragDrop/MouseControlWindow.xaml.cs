﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Kinect;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.CompilerServices;
using KinectEx.Smoothing;
using KinectEx;
using System.Windows.Controls;
using Microsoft.Samples.Kinect.SpeechBasics;

namespace KinectDragDrop
{
    /// <summary>
    /// MainWindow.xaml
    /// </summary>
    public partial class MouseControlWindow : System.Windows.Window, INotifyPropertyChanged
    {

        private Dictionary<string, Expander> panelDictionary = null;

        private KinectBodyView kinectBodyView = null;

        Dictionary<string, Border> menuDictionary = null;

        public void OldCode()
        {
            this.panelDictionary = new Dictionary<string, Expander>()
            {
                {"buildings", this.expBuildings},
                {"trees", this.expTrees},
                {"lights", this.expLights},
                {"cars", this.expCars},
            };

            this.menuDictionary = new Dictionary<string,Border>()
            {
               {"Menu0", this.Menu0},
               {"Menu1", this.Menu1},
               {"Menu2", this.Menu2},
               {"Menu3", this.Menu3},
               {"Menu4", this.Menu4},
               {"ToolBox", this.ToolBox},
            };

        }

        public void HideAllMenus()
        {
            foreach (var menu in this.menuDictionary.Values)
            {
                menu.Visibility = Visibility.Hidden;
            }
        }

        private void MenuButtonClicked(object sender, RoutedEventArgs e)
        {
            string tag = (string)((Button)sender).Tag;
            foreach (KeyValuePair<string, Expander> pair in this.panelDictionary)
            {
                if (pair.Key.Equals(tag))
                {
                    pair.Value.Visibility = Visibility.Visible;
                }
                else
                {
                    pair.Value.Visibility = Visibility.Collapsed;
                }
            }
            this.Menu4.Visibility = Visibility.Hidden;
            this.ToolBox.Visibility = Visibility.Visible;
        }

        private void ButtonClicked(object sender, RoutedEventArgs e)
        {
            string tag = (string)((Button)sender).Tag;
            if (null != tag && this.menuDictionary.ContainsKey(tag))
            {
                HideAllMenus();
                this.menuDictionary[tag].Visibility = Visibility.Visible;
            }
        }


        const int INPUT_MOUSE = 0;
        const int INPUT_KEYBOARD = 1;
        const int INPUT_HARDWARE = 2;
        const uint MOUSEEVENTF_MOVE = 0x0001;
        const uint MOUSEEVENTF_ABSOLUTE = 0x8000;
        const uint MOUSEEVENTF_LEFTDOWN = 0x02;
        const uint MOUSEEVENTF_LEFTUP = 0x04;

        enum ClickMode
        {
            Clench,
            Hold
        }

        struct INPUT
        {
            public int type;
            public InputUnion u;
        }

        [StructLayout(LayoutKind.Explicit)]
        struct InputUnion
        {
            [FieldOffset(0)]
            public MOUSEINPUT mi;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public uint mouseData;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);

        [StructLayout(LayoutKind.Sequential)]
        internal struct Win32Point
        {
            public Int32 X;
            public Int32 Y;
        };

        public static Point GetMousePosition()
        {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return new Point(w32Mouse.X, w32Mouse.Y);
        }

        struct HandInfo
        {
            public HandState HandState;
            public CameraSpacePoint Point;
            public IJoint HandJoint;
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetMessageExtraInfo();

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);

        //private float centerX = -1000;
        //private float centerY = -1000;

        private static int POINT_BUFFER_SIZE = 5;
        private int _pointBufferSize;

        /// <summary>
        /// In seconds
        /// </summary>
        public int HoldDelay
        {
            get
            {
                return _holdDelay;
            }
            set
            {
                if (value != _holdDelay)
                {
                    _holdDelay = value;
                    this.holdBuffer = new FixedSizedQueue<HandInfo>(_holdDelay * 60);
                }
            }
        }

        public int PointBufferSize
        {
            get { return _pointBufferSize; }
            set
            {
                _pointBufferSize = value;
                this.pointBuffer = new FixedSizedQueue<CameraSpacePoint>(_pointBufferSize * 2);
                this.pointBuffer2 = new FixedSizedQueue<CameraSpacePoint>(_pointBufferSize * 2);
                this.distanceBuffer = new FixedSizedQueue<PointF>(_pointBufferSize * 2);
            }
        }

        private FixedSizedQueue<PointF> distanceBuffer = null;
        private FixedSizedQueue<CameraSpacePoint> pointBuffer = null;
        private FixedSizedQueue<CameraSpacePoint> pointBuffer2 = null;

        private FixedSizedQueue<HandInfo> gestureBuffer = null;
        private FixedSizedQueue<HandInfo> holdBuffer = null;

        private CameraSpacePoint filtered;
        private static readonly float INFINITY = 10000;
        private CameraSpacePoint lastHandRight = new CameraSpacePoint() { X = INFINITY };
        private static int GESTURE_BUFFER_SIZE = 60;
        private static float HOLD_THRESHOLD_X = 0.01f;
        private static float HOLD_THRESHOLD_Y = 0.01f;
        private static float DISTANCE_FAULT_TOLERATE = 0.01f;
        private static float CLENCH_PERCENTILE = 0.1f;
        private static float LONG_CLENCH_PERCENTILE = 0.7f;
        private bool isClenched = false;
        private bool isLongClenched = false;
        private static float TRACKED_PERCENTILE = 0.9f;
        private float speedScale = 0.2f; // one centimet = 1 pixel;
        private HoldCursor holdCursor = null;


        public MouseControlWindow()
        {
            this.kinectSensor = KinectSensor.GetDefault();
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
            this.depthFrameReader = this.kinectSensor.DepthFrameSource.OpenReader();
            this.kinectSensor.Open();
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;
            this.depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
            this.displayWidth = depthFrameDescription.Width;
            this.displayHeight = depthFrameDescription.Height;
            this.realTimeImage = new WriteableBitmap(this.displayWidth, this.displayHeight, 96.0, 96.0, PixelFormats.Gray8, null);
            this.bodyArray = new Body[this.kinectSensor.BodyFrameSource.BodyCount];
            this.drawingGroup = new DrawingGroup();
            this.imageSource = new DrawingImage(this.drawingGroup);
            // allocate space to put the pixels being received and converted
            this.depthPixels = new byte[this.depthFrameDescription.Width * this.depthFrameDescription.Height];
            this.DataContext = this;
            this.PointBufferSize = POINT_BUFFER_SIZE;
            this.customBodies = new List<CustomBody>();
            this.kalmanBodies = new SmoothedBodyList<KalmanSmoother>();
            this.exponentialBodies = new SmoothedBodyList<ExponentialSmoother>();
            this.HoldDelay = 1;
            this.gestureBuffer = new FixedSizedQueue<HandInfo>(GESTURE_BUFFER_SIZE);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += bodyFrameReader_FrameArrived;
            }
            if (this.depthFrameReader != null)
            {
                this.depthFrameReader.FrameArrived += depthFrameReader_FrameArrived;
            }
            this.holdCursor = new HoldCursor();
            this.holdCursor.Owner = this;
            this.holdCursor.Animator.AnimationFinished += Animator_AnimationFinished;

            // initialize the BodyViewer object for displaying tracked bodies in the UI
            this.kinectBodyView = new KinectBodyView(this.kinectSensor);
            // set our data context objects for display in UI
            this.DataContext = this;
            this.kinectBodyViewbox.DataContext = this.kinectBodyView;


            OldCode();
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        private void depthFrameReader_FrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            bool depthFrameProcessed = false;

            using (DepthFrame depthFrame = e.FrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    // the fastest way to process the body index data is to directly access 
                    // the underlying buffer
                    using (Microsoft.Kinect.KinectBuffer depthBuffer = depthFrame.LockImageBuffer())
                    {
                        // verify data and write the color data to the display bitmap
                        if (((this.depthFrameDescription.Width * this.depthFrameDescription.Height) == (depthBuffer.Size / this.depthFrameDescription.BytesPerPixel)) &&
                            (this.depthFrameDescription.Width == this.realTimeImage.PixelWidth) && (this.depthFrameDescription.Height == this.realTimeImage.PixelHeight))
                        {
                            // Note: In order to see the full range of depth (including the less reliable far field depth)
                            // we are setting maxDepth to the extreme potential depth threshold
                            ushort maxDepth = ushort.MaxValue;

                            // If you wish to filter by reliable depth distance, uncomment the following line:
                            //// maxDepth = depthFrame.DepthMaxReliableDistance

                            this.ProcessDepthFrameData(depthBuffer.UnderlyingBuffer, depthBuffer.Size, depthFrame.DepthMinReliableDistance, maxDepth);
                            depthFrameProcessed = true;
                        }
                    }
                }
            }

            if (depthFrameProcessed)
            {
                this.RenderDepthPixels();
            }
        }

        /// <summary>
        /// Directly accesses the underlying image buffer of the DepthFrame to 
        /// create a displayable bitmap.
        /// This function requires the /unsafe compiler option as we make use of direct
        /// access to the native memory pointed to by the depthFrameData pointer.
        /// </summary>
        /// <param name="depthFrameData">Pointer to the DepthFrame image data</param>
        /// <param name="depthFrameDataSize">Size of the DepthFrame image data</param>
        /// <param name="minDepth">The minimum reliable depth value for the frame</param>
        /// <param name="maxDepth">The maximum reliable depth value for the frame</param>
        private unsafe void ProcessDepthFrameData(IntPtr depthFrameData, uint depthFrameDataSize, ushort minDepth, ushort maxDepth)
        {
            // depth frame data is a 16 bit value
            ushort* frameData = (ushort*)depthFrameData;

            // convert depth to a visual representation
            for (int i = 0; i < (int)(depthFrameDataSize / this.depthFrameDescription.BytesPerPixel); ++i)
            {
                // Get the depth for this pixel
                ushort depth = frameData[i];

                // To convert to a byte, we're mapping the depth value to the byte range.
                // Values outside the reliable depth range are mapped to 0 (black).
                this.depthPixels[i] = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0);
            }
        }

        /// <summary>
        /// Renders color pixels into the writeableBitmap.
        /// </summary>
        private void RenderDepthPixels()
        {
            this.realTimeImage.WritePixels(
                new Int32Rect(0, 0, this.realTimeImage.PixelWidth, this.realTimeImage.PixelHeight),
                this.depthPixels,
                this.realTimeImage.PixelWidth,
                0);
        }

        void bodyFrameReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;
            IEnumerable<IBody> bodies = null;
            using (DrawingContext dc = this.drawingGroup.Open())
            {
                // Draw a transparent background to set the render size

                dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                // draw the kinect bitmap if it's there
                if (null != RealTimeImage)
                {
                    // determine the coordinates for displaying the image
                    Double w = realTimeImage.Width * this.displayHeight / realTimeImage.Height;
                    Double diffWidth = Math.Abs(this.displayWidth - w);
                    Double x = -(diffWidth / 2);
                    Double ww = w + x;
                    dc.DrawImage(realTimeImage, new Rect(x, 0.0, w, this.displayHeight));
                }

                using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
                {
                    if (bodyFrame != null)
                    {
                        bodyFrame.GetAndRefreshBodyData(this.kalmanBodies);
                        bodies = this.kalmanBodies;
                        var b = new Body[bodyFrame.BodyCount];
                        bodyFrame.GetAndRefreshBodyData(b);

                        this.kinectBodyView.UpdateBodyFrame(b);
                        foreach (IBody body in bodies)
                        {
                            if (body.IsTracked)
                            {
                                // convert the joint points to depth (display) space
                                Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                foreach (JointType jointType in body.Joints.Keys)
                                {
                                    DepthSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(body.Joints[jointType].Position);
                                    jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
                                }
                                this.DrawBody(body.Joints, jointPoints, dc);
                                this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);
                            }
                        }
                        // prevent drawing outside of our render area
                        this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                        dataReceived = true;
                    }
                }
            }
            if (dataReceived)
            {
                var body = bodies.Where(b => b.IsTracked && (this.trackedId == null || b.TrackingId == this.trackedId.Value)).FirstOrDefault();
                if (body == null)
                {
                    this.trackedId = null;
                }
                else
                {
                    this.trackedId = body.TrackingId;
                    var point = body.Joints[JointType.HandRight].Position;
                    var leftButtonUp = false;
                    var leftButtonDown = false;
                    var toMove = true;
                    var h = new HandInfo
                    {
                        HandState = body.HandRightState,
                        Point = point,
                        HandJoint = body.Joints[JointType.HandRight]
                    };
                    this.gestureBuffer.Enqueue(h);
                    this.holdBuffer.Enqueue(h);
                    switch (this.clickMode)
                    {
                        case ClickMode.Hold:
                            CheckHold();
                            if (this.leftClick)
                            {
                                leftButtonUp = true;
                                leftButtonDown = true;
                                this.leftClick = false;
                            }
                            break;
                        case ClickMode.Clench:
                            if (this.holdCursor.IsVisible)
                            {
                                this.holdCursor.Hide();
                            }
                            var clenched = CheckClench();
                            var longClenched = CheckLongClench();
                            var handTracked = CheckHand();
                            leftButtonUp = (isClenched | isLongClenched) && (!clenched);
                            leftButtonDown = clenched && !this.isClenched;
                            this.isClenched = clenched;
                            this.isLongClenched = longClenched;

                            if ((clenched && !longClenched) | !handTracked | h.HandState == HandState.Unknown)
                            {
                                toMove = false;
                                this.lastX = INFINITY;
                            }

                            else
                            {
                                //point = Accelerate(point);
                            }
                            if ((clenched && !longClenched) && (this.pointBuffer.Count > 0))
                            {
                                this.pointBuffer.Clear();
                            }
                            break;
                    }

                    var inputs = new List<INPUT>();

                    pointBuffer.Enqueue(point);

                    if (toMove && this.pointBuffer.Count >= POINT_BUFFER_SIZE)
                    {
                        filtered = SimpleAverageFilter();

                        float x = filtered.X;
                        float y = filtered.Y;

                        if (this.lastX == INFINITY)
                        {
                            this.lastX = x;
                            this.lastY = y;
                        }
                        var deltaX = x - this.lastX;
                        var deltaY = -(y - this.lastY);

                        this.lastX = x;
                        this.lastY = y;

                        inputs.Add(new INPUT
                        {
                            type = INPUT_MOUSE,
                            u = new InputUnion
                            {
                                mi = new MOUSEINPUT
                                {
                                    dwFlags = MOUSEEVENTF_MOVE,
                                    dx = (int)(65535 * deltaX * speedScale),
                                    dy = (int)(65535 * deltaY * speedScale),
                                }
                            }
                        });
                    }

                    if (leftButtonUp | leftButtonDown)
                    {
                        inputs.Add(new INPUT
                        {
                            type = INPUT_MOUSE,
                            u = new InputUnion
                            {
                                mi = new MOUSEINPUT
                                {
                                    dwFlags = (leftButtonDown ? MOUSEEVENTF_LEFTDOWN : 0x0) |
                                        (leftButtonUp ? MOUSEEVENTF_LEFTUP : 0x0)
                                }
                            }
                        });
                    }

                    var inputArray = inputs.ToArray();
                    SendInput((uint)inputArray.Length, inputArray, Marshal.SizeOf(typeof(INPUT)));
                }
            }
        }

        private void NeutralizeQueue()
        {
            throw new NotImplementedException();
        }

        private bool CheckHand()
        {
            if (gestureBuffer.Where(x => x.HandJoint.TrackingState == TrackingState.Tracked).Count() < GESTURE_BUFFER_SIZE * TRACKED_PERCENTILE) return false;
            return true;
        }

        private bool CheckClench()
        {
            if (gestureBuffer.Where(x => x.HandState == HandState.Closed).Count() < GESTURE_BUFFER_SIZE * CLENCH_PERCENTILE) return false;
            return true;//Not qualified for hold
        }

        private bool CheckLongClench()
        {
            if (gestureBuffer.Where(x => x.HandState == HandState.Closed).Count() < GESTURE_BUFFER_SIZE * LONG_CLENCH_PERCENTILE) return false;
            return true;//Not qualified for hold
        }

        private void CheckHold()
        {
            if (this.holdBuffer.Count < this.HoldDelay * 60) return;

            // CHeck for threshold
            bool isHold = false;

            var listX = holdBuffer.Select(h => (double)h.Point.X).ToList();
            var stdX = listX.StandardDeviation();
            var listY = holdBuffer.Select(h => (double)h.Point.Y).ToList();
            var stdY = listY.StandardDeviation();
            var fault = holdBuffer.Where(h => h.HandState != HandState.Open).Count();
            isHold = (fault < this.HoldDelay * 60 * DISTANCE_FAULT_TOLERATE) && (stdX < HOLD_THRESHOLD_X) && (stdY < HOLD_THRESHOLD_Y);

            if (isHold)
            {
                var cursorPostion = GetMousePosition();
                var cursorWidth = this.holdCursor.Width;
                var cursorHeight = this.holdCursor.Height;
                this.holdCursor.Left = cursorPostion.X - (cursorWidth / 2);
                this.holdCursor.Top = cursorPostion.Y - (cursorHeight / 2);
                this.holdCursor.Show();
            }
            else
            {
                if (this.holdCursor.IsVisible)
                {
                    this.holdCursor.Hide();
                    this.holdBuffer.Clear();
                }
            }
        }

        void Animator_AnimationFinished(object sender, EventArgs e)
        {
            if (this.holdCursor.IsVisible)
            {
                this.holdCursor.Hide();
                this.leftClick = true;
                this.holdBuffer.Clear();
                //throw new Exception();
            }
        }

        private Point Accelerate(Point delta)
        {
            //(1/(0.5+4))*  x*(Abs(x)+4)
            float linearity = 0.3F;   // higher values mean more linear curve, less acceleration
            float intersection = 0.05F; // point where accelerated curve intersects with linear curve

            //Formula:(with Mathf.Abs we can keep the minus for negative values after the multiplication)
            double curMovX = (1F / (intersection + linearity)) * delta.X * (Math.Abs(delta.X) * linearity);
            double curMovY = (1F / (intersection + linearity)) * delta.Y * (Math.Abs(delta.Y) * linearity);

            //Clamp if needed
            curMovX = Math.Min(Math.Max(curMovX, -linearity), linearity);
            curMovY = Math.Min(Math.Max(curMovY, -linearity), linearity);
            return new Point { X = curMovX, Y = curMovY };
        }

        private CameraSpacePoint SimpleAverageFilter()
        {
            CameraSpacePoint ret = new CameraSpacePoint();
            pointBuffer.TryPeek(out ret);
            return ret;
        }

        #region Private members
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for body
        /// frames
        /// </summary>
        private DepthFrameReader depthFrameReader = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodyArray = null;

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        private WriteableBitmap realTimeImage = null;
        /// <summary>
        /// Intermediate storage for frame data converted to color
        /// </summary>
        private byte[] depthPixels = null;
        private FrameDescription depthFrameDescription;
        /// <summary>
        /// Map depth range to byte range
        /// </summary>
        private const int MapDepthToByte = 8000 / 256;
        #endregion

        #region Canvas Drawing Methods
        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);


                    break;
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, IJoint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            var joint0 = joints[jointType0];
            var joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, IJoint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }
        #endregion


        public WriteableBitmap RealTimeImage
        {
            get { return realTimeImage; }
            set
            {
                realTimeImage = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private float lastX = INFINITY;
        private float lastY = INFINITY;
        private ClickMode clickMode = ClickMode.Clench;
        private bool leftClick = false;
        private SmoothedBodyList<KalmanSmoother> kalmanBodies;
        private SmoothedBodyList<ExponentialSmoother> exponentialBodies;
        private List<CustomBody> customBodies;
        private ulong? trackedId;
        private int _holdDelay;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
        }
    }
}
